Ansible Role to manage ssh parameters
-------------------------------------

This simple role allow you to manage SSH installation / configuration.

Default behaviour is to change the default port to the `ansible_port` value.

## Example

```ansible-playbook
- name: Configure SSH
  include_role:
    name: ssh
  vars:
    check_connect_timeout: 2
    check_timeout: 3
```
